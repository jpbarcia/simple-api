FROM alpine

RUN apk update && apk add --no-cache python3 
RUN apk add --no-cache --virtual .pynacl_deps build-base python3-dev libffi-dev openssl openssl-dev
RUN pip3 install --upgrade pip

COPY . /home/app
RUN pip3 install -r /home/app/requirements.txt

CMD [ "/home/app/run.sh" ]
EXPOSE 80