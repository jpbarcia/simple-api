from flask import Flask, jsonify, g, request, abort
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import BadRequest
app = Flask(__name__)
from models import database_config
from models.product import Product
database_config(app)


@app.route("/")
def home():
    return "Home path"


@app.route("/product", methods=['POST'])
def post():
    json = request.get_json()
    product = Product()
    product.name = json['name']
    product.stock = json['stock']

    try:
        g.db.add(product)
        g.db.commit()
    except IntegrityError as err:
        abort(BadRequest.code, "Error posting the product ")

    return jsonify(product.as_dict())


@app.route("/product/<int:id>", methods=['GET'])
def one(id):
    product = g.db.query(Product).filter_by(id=id).first()
    if product is None:
        abort(BadRequest.code, "Product doesn't exist.")
    return jsonify(product.as_dict())


@app.route("/product/page/<int:page>", methods=['GET'])
def all(page):
    q_per_page = 10
    offset = (page - 1) * 10
    products = g.db.query(Product).\
        order_by(Product.id).\
        slice(offset, offset + q_per_page)
    return jsonify([p.as_dict() for p in products])
